# Fichier de règles

### Formalisme ###
# Commentaire : ligne commencant par #
# Règle : condition ^ condition -> conclusion ^ conclusion
# création d'un noeud : new nom1-nom2 variable

### Lemmes ###
$x r_succ $z ^ $x r_lemma $y -> $y r_succ $z
$x r_succ $z ^ $z r_lemma $y -> $x r_succ $y

### Reflexivité des predécesseurs et successeurs ###
$x r_succ $y -> $y r_prec $x
$x r_prec $y -> $y r_succ $x

### Transitivité des relation r_agent_jdm et r_patient_jdm et r_idee_jdm ###
$x r_isa $y ^ $y r_agent_jdm $a -> $x r_agent_jdm $a
$x r_isa $y ^ $y r_patient_jdm $a -> $x r_patient_jdm $a
$x r_isa $y ^ $y r_idee_jdm $a -> $x r_idee_jdm $a

$x r_patient_jdm $y -> $x r_idee_jdm $y
$x r_agent_jdm $y -> $x r_idee_jdm $y


### Mot interrogatif ###
$x r_pos Pro:Interro -> $x r_pos Mot:Interro:
$x r_pos Int -> $x r_pos Mot:Interro:

### GN ###

## Nom ##
$n r_pos Nom: ^ $n r_prec $p ^ $n r_succ $s -> new Nom:-$n $gn ^ $gn r_pos GN: ^ $gn r_head $n ^ $gn r_prec $p ^ $gn r_succ $s
$n r_pos Nom: ^ $n r_prec $p ^ $n r_fin Fin: -> new Nom:-$n $gn ^ $gn r_pos GN: ^ $gn r_head $n ^ $gn r_prec $p ^ $gn r_fin Fin:

## Nom propre ##
$np r_pos Nom:Propre ^ $np r_prec $p ^ $np r_succ $s -> new NomPropre:-$np $gn ^ $gn r_pos GNDET: ^ $gn r_head $np ^ $gn r_prec $p ^ $gn r_succ $s
$np r_pos Nom:Propre ^ $np r_prec $p ^ $np r_fin Fin: -> new NomPropre:-$np $gn ^ $gn r_pos GNDET: ^ $gn r_head $np ^ $gn r_prec $p ^ $gn r_fin Fin:
$np r_pos Nom:Propre ^ $np r_debut Debut: ^ $np r_succ $s -> new NomPropre:-$np $gn ^ $gn r_pos GNDET: ^ $gn r_head $np ^ $gn r_debut Debut: ^ $gn r_succ $s

## Det GN ##
$d r_pos Det: ^ $d r_succ $gn ^ $gn r_pos GN: ^ $d r_prec $p ^ $gn r_succ $s ^ $gn r_head $h -> new Det:-$d-$gn $x ^ $x r_pos GNDET: ^ $x r_head $h ^ $x r_succ $s ^ $x r_prec $p ^ $x r_contient $gn
$d r_pos Det: ^ $d r_succ $gn ^ $gn r_pos GN: ^ $gn r_fin Fin: ^ $d r_prec $p ^ $gn r_head $h -> new Det:-$d-$gn $x ^ $x r_pos GNDET: ^ $x r_head $h ^ $x r_prec $p ^ $x r_fin Fin: ^ $x r_contient $gn
$d r_pos Det: ^ $d r_debut Debut: ^ $d r_succ $gn ^ $gn r_pos GN: ^ $gn r_succ $s ^ $gn r_head $h -> new Det:-$d-$gn $x ^ $x r_pos GNDET: ^ $x r_head $h ^ $x r_succ $s ^ $x r_debut Debut: ^ $x r_contient $gn

## GNDET Adj ##
$gn r_pos GNDET: ^ $gn r_succ $adj ^ $adj r_pos Adj: ^ $gn r_prec $p ^ $adj r_succ $s ^ $gn r_head $h -> new $gn-Adj:-$adj $x ^ $x r_pos GNDET: ^ $x r_head $h ^ $x r_carac $adj ^ $x r_succ $s ^ $x r_prec $p ^ $x r_contient $gn
$gn r_pos GNDET: ^ $gn r_succ $adj ^ $adj r_pos Adj: ^ $adj r_fin Fin: ^ $gn r_prec $p ^ $gn r_head $h -> new $gn-Adj:-$adj $x ^ $x r_pos GNDET: ^ $x r_head $h ^ $x r_carac $adj ^ $x r_fin Fin: ^ $x r_prec $p ^ $x r_contient $gn
$gn r_pos GNDET: ^ $gn r_debut Debut: ^ $gn r_succ $adj ^ $adj r_pos Adj: ^ $adj r_succ $s ^ $gn r_head $h -> new $gn-Adj:-$adj $x ^ $x r_pos GNDET: ^ $x r_head $h ^ $x r_carac $adj ^ $x r_debut Debut: ^ $x r_succ $s ^ $x r_contient $gn

## Adj GN ##
$gn r_pos GN: ^ $gn r_prec $adj ^ $adj r_pos Adj: ^ $adj r_prec $p ^ $gn r_succ $s ^ $gn r_head $h -> new Adj:-$adj-$gn $x ^ $x r_pos GN: ^ $x r_head $h ^ $x r_carac $adj ^ $x r_succ $s ^ $x r_prec $p ^ $x r_contient $gn
$gn r_pos GN: ^ $gn r_prec $adj ^ $adj r_pos Adj: ^ $gn r_fin Fin: ^ $adj r_prec $p ^ $gn r_head $h -> new Adj:-$adj-$gn $x ^ $x r_pos GN: ^ $x r_head $h ^ $x r_carac $adj ^ $x r_fin Fin: ^ $x r_prec $p ^ $x r_contient $gn

## Liaison des carac des GN ##
$x r_contient $y ^ $y r_carac $c -> $x r_carac $c


### GV ###
$x r_pos Ver: ^ $x r_lemma $v -> $x r_pos GV: ^ $x r_verbe $v ^ $x r_verbe $x
$x r_pos Ver: -> $x r_pos GV: ^ $x r_verbe $x


### Question ###

## Mot:Interro: GV GNDET
$i r_pos Mot:Interro: ^ $i r_debut Debut: ^ $i r_succ $gv ^ $gv r_pos GV: ^ $gv r_succ $gn ^ $gn r_fin Fin: ^ $gn r_pos GNDET: -> new Interro:-$i-Ver:-$gv-$gn $x ^ $x r_pos Question: ^ $x r_interro $i ^ $x r_action $gv ^ $x r_patient $gn

## Mot:Interro: GV
$i r_pos Mot:Interro: ^ $i r_debut Debut: ^ $i r_succ $gv ^ $gv r_pos GV: ^ $gv r_fin Fin: -> new Interro:-$i-Ver:-$gv $x ^ $x r_pos Question: ^ $x r_interro $i ^ $x r_action $gv
