let nodeCount = 0
let vertexCount = 0

// Graph class
function Graph() {

    this.nodes = []
    this.vertices = []

    this.addNode = function(name) {
        let new_node = new GraphNode(name)
        this.nodes.push(new_node)
        return new_node
    }

    this.addNode("GN:")
    this.addNode("GNDET:")
    this.addNode("GV:")
    this.addNode("Mot:Interro:")
    this.addNode("Phrase:")
    this.addNode("Question:")

    this.addVertex = function(start, end, type, rule, weight) {
        if(this.startAndEndNodeExist(start, end)) {
            if(this.vertices.find(v => (
                v.initialNode == start
                && v.endNode == end
                && v.type == type
                && (v.weight == weight || 1))
            ) === undefined) {
                let new_vertex = new GraphVertex(start, end, type, rule, weight)
                this.vertices.push(new_vertex)
                start.addOutVertex(new_vertex)
                end.addInVertex(new_vertex)
                return new_vertex
            }
        }
        return undefined
    }

    this.startAndEndNodeExist = function(start, end) {
        return (this.nodes.indexOf(start) >= 0 && this.nodes.indexOf(end) >= 0)
    }

    this.apply = function(rule) {        
        let homomorphismes = backtrackAll(this.vertices, {}, rule, 0, [])  
     
        let changed = false
        for(h of homomorphismes) {
            for(part of rule.conclusions) {
                if(part.initialNode == "new") {
                    let new_node_name = ""
                    let variables_in_name = part.vertexType.split("-")
                    for(v of variables_in_name) {
                        if(v.startsWith('$')) {
                            new_node_name += h[v].label
                        } else {
                            new_node_name += v
                        }
                    }
                    let end_node = this.nodes.find(n => n.label === new_node_name)
                    if(end_node === undefined) {
                        h[part.endNode] = this.addNode(new_node_name)
                    } else {
                        h[part.endNode] = end_node
                    }
                } else {
                    let end = undefined
                    if(part.endNode.startsWith("$")) {
                        end = h[part.endNode]
                    } else {
                        end = this.nodes.find(n => n.label === part.endNode)
                    }
                    let new_vertex = this.addVertex(h[part.initialNode], end, part.vertexType, {rule, h})               
                    changed = changed || new_vertex !== undefined
                }
            }
        }

        return changed
    }
}

const backtrackAll = function(all_vertices, old_homomorphism, rule, condition_index, res) {
    const current_condition = rule.conditions[condition_index]
    const vertices_with_correct_type = all_vertices.filter(vertex => vertex.type === current_condition.vertexType && vertex.weight > 0)

    for(vertex of vertices_with_correct_type) {
        let changing_homomorphism = Object.assign({}, old_homomorphism) // copy
        let $x = changing_homomorphism[current_condition.initialNode]
        let $y = changing_homomorphism[current_condition.endNode]

        // if homomorphism is correct (no conflict created by new assignment) and constants are correct
        // startsWith remple includes
        // replace("'","")
        if(((
                $x === undefined &&
                current_condition.initialNode[0] !== "$" &&
                vertex.initialNode.label.replace("'","").startsWith(current_condition.initialNode.replace("'",""))
            ) || (
                $x === undefined &&
                current_condition.initialNode[0] === "$"
            ) || ($x === vertex.initialNode))
            && ((
                $y === undefined &&
                current_condition.endNode[0] !== "$" &&
                vertex.endNode.label.replace("'","").startsWith(current_condition.endNode.replace("'",""))
            ) || (
                $y === undefined &&
                current_condition.endNode[0] === "$"
            ) || ($y === vertex.endNode))) {

            changing_homomorphism[current_condition.initialNode] = vertex.initialNode
            changing_homomorphism[current_condition.endNode] = vertex.endNode
            if(condition_index === rule.conditions.length -1) {
                // full homomorphism found
                res.push(Object.assign({}, changing_homomorphism))
            } else {
                // continue on current homomorphism
                res = backtrackAll(all_vertices, changing_homomorphism, rule, condition_index + 1, res)
            }
        }
    }
    return res
}

// Node Class
function GraphNode(name) {

    this.label = name

    this.outVertices = []
    this.inVertices = []

    this.id = "Node:" + nodeCount
    nodeCount++

    this.addOutVertex = function(vertex) {
        this.outVertices.push(vertex)
    }

    this.addInVertex = function(vertex) {
        this.inVertices.push(vertex)
    }

    this.toString = function() {
        return this.label
    }

}

// Vertex Class
// TODO : rule en paramètre
function GraphVertex(start, end, type, rule = 'règle inconnue',  weight = '1') {

    this.initialNode = start
    this.endNode = end
    this.type = type
    this.weight = weight
    this.rule = rule

    this.id = "Egde:" + vertexCount
    vertexCount++

    this.toString = function() {
        return this.initialNode + " --> " + this.type + " --> " + this.endNode
    }

}
