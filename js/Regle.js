function Rule(conditions, conclusions) {
    this.conditions = conditions
    this.conclusions = conclusions
}

function RulePart(start, type, end) {
    this.initialNode = start
    this.endNode = end
    this.vertexType = type
}

function ruleFromRawLine(text) {
    const splited_text = text.split(/[ ]*\-\>[ ]*/gi)
    const conditions = splited_text[0].split(/[ ]*\^[ ]*/gi).map(r => rulePartFromRawRule(r))
    const conclusions = splited_text[1].split(/[ ]*\^[ ]*/gi).map(r => rulePartFromRawRule(r))

    return new Rule(conditions, conclusions)
}

function rulePartFromRawRule(text) {
    const rule = text.split(/[ ]+/gi)
    return new RulePart(rule[0], rule[1], rule[2])
}