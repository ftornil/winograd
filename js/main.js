const JDM_URL = 'http://localhost:5000/data?mot=%s&rel=%s'
const LOCAL_URL = 'http://localhost:5000'

// global graph
var sentenceGraph
var questionGraph

// Number of currently running request
var runningRequest = 0

// events
document.addEventListener('AllRequestQuestionEnded', function(){ appliquerRegles(true); })
document.addEventListener('AllRequestEnded', appliquerRegles)

// Print text at the bottom of the html page
function debug(text) {
    document.getElementById('debug_text').innerHTML += "<p>"+text+"</p>"
}

// Analyse la phrase saisie par l'utilisateur
// Crée le graphe correspondant
// Contacte JDM pour obtenir les informations manquantes sur les mots
// Applique l'ensemble des règles
function analyse(){

    document.getElementById("log_phrase").innerHTML = "<hr> <p> Analyse de la phrase en cours ... Requêtage</p>"

    let texteAsArray = document.getElementById('phrase').value.replace(',', ' , ').replace('\'', '\' ').split(/[ ]+/gi)

    generateGraph(texteAsArray)
    
    // Requêtes initiales vers JDM
    let deja_vu = []
    for (let mot of texteAsArray) {
        if(deja_vu.indexOf(mot) == -1) {
            deja_vu.push(mot)
            requestJDM(mot, 4) // POS
            requestJDM(mot, 19) // Lemmes
            requestJDM(mot, 13) // r_agent (ce que peut faire mot)
            requestJDM(mot, 14) // r_patient (ce qu'on peut faire à mot)
            requestJDM(mot, 18) // r_data
        }
    }
}

// Génère le graph contenant :
// * Noeuds = mots de la phrase
// * Arcs = r_succ d'un mot vers sont successeur dans la prhase
//          r_prec d'un mot vers sont précédant dans la phrase
function generateGraph(phrase, is_question_graph = false) {

    let graph = undefined
    if (is_question_graph == true){
        questionGraph = new Graph()
        graph = questionGraph
    }else{
        sentenceGraph = new Graph()
        graph = sentenceGraph
    }

    let debut = graph.addNode("Debut:")
    let fin = graph.addNode("Fin:")

    let previous_node = undefined
    let mot = ""

    for(let i=0; i<phrase.length; i++){
        mot = phrase[i]
        
        let current_node = graph.addNode(mot)
        if(previous_node !== undefined) {
            graph.addVertex(previous_node, current_node, "r_succ", "regle r_succ")
            graph.addVertex(current_node, previous_node, "r_prec", "regle r_prec")
        }

        if (i==0){
            graph.addVertex(current_node, debut, "r_debut")
        } else if (i == phrase.length-1){
            graph.addVertex(current_node, fin, "r_fin")
        }
        
        previous_node = current_node
    }
}

// Requête sur la plateforme JeuxDeMots un terme avec un relation associée
function requestJDM(terme, relation, is_question_graph = false) {
    let url = JDM_URL.replace('%s', terme).replace('%s', relation)

    let request = new XMLHttpRequest();
    request.open('GET', url, true)
    request.overrideMimeType('text/html')

    runningRequest++
    
    request.onload = function(res) {
        traiterReponseJDM(request.responseText, terme, relation, is_question_graph)
        runningRequest--
        if(runningRequest <= 0) {
            if (is_question_graph == true){
                document.dispatchEvent(new Event('AllRequestQuestionEnded'))
            }else{
                document.dispatchEvent(new Event('AllRequestEnded'))
            }
        }
    }
    request.send(null)

}

// Traite une réponse provenant de JeuxDeMots en récupérant les lignes nécessaires au traitement
function traiterReponseJDM(reponseHTML, terme, relation_cherchee, is_question_graph = false) {
    let indice_debut = reponseHTML.search("<CODE>")

    if(indice_debut == -1){
        //console.log("indice debut relation == -1")
        return
    }else{
        indice_debut += 6
        let texte = reponseHTML.substring(indice_debut, reponseHTML.length)

        // Entités
        let debut_entite_string = "'formated name'"
        let debut_entites = texte.search(debut_entite_string) + debut_entite_string.length
        if(debut_entites == 0) { 
            console.log("pas d'entites")
            return 
        }

        texte = texte.substring(
            debut_entites+3,
            texte.length)
        let fin_des_entites = texte.search('\n\n//')
        let entites_texte = texte.substring(0, fin_des_entites)
        texte = texte.substring(fin_des_entites)


        // Relations sortantes 
        let debut_relations_out_string = "type;w"
        let debut_relations_out = texte.search(debut_relations_out_string) + debut_relations_out_string.length
        texte = texte.substring(
            debut_relations_out+2,
            texte.length)
        let fin_des_relations_out = texte.search('\n\n//')
        let relations_out_texte = texte.substring(0, fin_des_relations_out)
        texte = texte.substring(fin_des_relations_out)


        // Relations entrantes
        let debut_relations_in_string = "type;w"
        let debut_relations_in = texte.search(debut_relations_in_string) + debut_relations_in_string.length
        texte = texte.substring(
            debut_relations_in+2,
            texte.length)
        let fin_des_relations_in = texte.search('\n\n//')
        let relations_in_texte = texte.substring(0, fin_des_relations_in) 

        let entites = entites_texte.split('\n')
        let relations_in = relations_in_texte.split('\n')
        relations_in.shift()
        let relations_out = relations_out_texte.split('\n')
        relations_out.shift()

        if (relations_in.length == 0 && relations_out.length == 0){
            //console.log("pas de relations")
            return
        }        

        // Lien entre entité et relation
        let new_relations_in = {}
        let new_relations_out = {}

        // On rempli le stableaux relations avec les entites
        // e[1] = id de la relation
        // e[2]/e[5] = le nom 

        if (relations_in.length == 0){
            for(entite of entites) {
                let e = entite.split(';')
                if (e[5] != undefined){
                    new_relations_out[e[1]] = {'nom': e[5]}
                } else { 
                    new_relations_out[e[1]] = {'nom': e[2]}
                }
            }
        } else if(relations_out.length == 0){
            for(entite of entites) {
                let e = entite.split(';')
                if (e[5] != undefined){
                    new_relations_in[e[1]] = {'nom': e[5]}
                } else { 
                    new_relations_in[e[1]] = {'nom': e[2]}
                }
            }
        } else{
            for(entite of entites) {
                // Nom usuel s'il existe
                let e = entite.split(';')
                if (e[5] != undefined){
                    new_relations_in[e[1]] = {'nom': e[5]}
                    new_relations_out[e[1]] = {'nom': e[5]}
                } else { // Sinon l'autre
                    new_relations_in[e[1]] = {'nom': e[2]}
                    new_relations_out[e[1]] = {'nom': e[2]}
                }
            }
        }

        // On donne un poids
        // // r[4] = le numéro de la relation
        // // r[2] == id
        for(relation of relations_out) {
            let r = relation.split(';')
            if (r[4] == relation_cherchee) {
                try {new_relations_out[r[3]].poids = r[5]} catch(e) {/*console.log(reponseHTML);*/}
            }
        }

        // // r[3] = id
        for(relation of relations_in) {
            let r = relation.split(';')
            if (r[4] == relation_cherchee) {
                try {new_relations_in[r[2]].poids = r[5]} catch(e) {/*console.log(reponseHTML);*/}
            }
        }
       
        // On supprime ce qui ne devrait pas être là
        for(let i in new_relations_in){
            if(new_relations_in[i].poids == undefined){
                delete new_relations_in[i]
            } else {
                let relation_label = new_relations_in[i].nom.slice(1, new_relations_in[i].nom.length -1)
                new_relations_in[i].nom = relation_label
            }
        }

        for(let i in new_relations_out){
            if(new_relations_out[i].poids == undefined){
                delete new_relations_out[i]
            } else {
                let relation_label = new_relations_out[i].nom.slice(1, new_relations_out[i].nom.length -1)
                new_relations_out[i].nom = relation_label
            }
        }

        // Ajout des relations dans le graphe
        // in ou out
        if(relation_cherchee == 4) {
            if(terme=="où") {
                new_relations_out[-133] = {poids: 666, nom: "Pro:Interro"}
            }
            addRelationToGraph(new_relations_out, terme, 'r_pos', is_question_graph)
        }

        if(relation_cherchee == 19){
            addRelationToGraph(new_relations_out, terme, 'r_lemma', is_question_graph)
        }

        if(relation_cherchee == 13){
            addRelationToGraph(new_relations_in, terme, 'r_agent_jdm', is_question_graph)
        }

        if(relation_cherchee == 14){
            addRelationToGraph(new_relations_in, terme, 'r_patient_jdm', is_question_graph)
        }

        if(relation_cherchee == 14){
            addRelationToGraph(new_relations_out, terme, 'r_patient_jdm', is_question_graph)
        }

        if(relation_cherchee == 18){
            for(let i in new_relations_out){
                if(new_relations_out[i].nom == 'Nom:Propre') {
                    addRelationToGraph([new_relations_out[i]], terme, 'r_pos', is_question_graph)
                    requestJDM(terme, 8, is_question_graph) // r_isa (spécifique)
                }
            }
        }

        if(relation_cherchee == 8) {
            addRelationToGraph(new_relations_in, terme, 'r_isa', is_question_graph)
            for(let i in new_relations_in){
                requestJDM(new_relations_in[i].nom, 13, is_question_graph) // r_agent
                requestJDM(new_relations_in[i].nom, 14, is_question_graph) // r_patient
            }
        }

        if(relation_cherchee == 0) {
            addRelationToGraph(new_relations_out, terme, 'r_idee_jdm', is_question_graph)
        }
    }

}

// Ajoute les relations données dans le graphe représentant la phrase
function addRelationToGraph(new_relations, terme, relation_name, is_question_graph = false) {
    let graph = undefined
    if (is_question_graph == true){
        graph = questionGraph
    }else{
        graph = sentenceGraph
    }

    let occurences = []
    graph.nodes.forEach(node => {
        if(node.label == terme) {
            occurences.push(node)
        }
    })

    for(let i in new_relations){
        let new_relation = new_relations[i]
        let node_tag = graph.nodes.find(node => node.label == new_relation.nom) || graph.addNode(new_relation.nom)
        for(node of occurences) {
            graph.addVertex(node, node_tag, relation_name, "regles main.js", new_relation.poids)
        }
    }
}

// Charge le fichier règles.txt puis lance le traitement de ce fichier
async function appliquerRegles(is_question_graph = false) {

    if (is_question_graph == true){
        document.getElementById("log_question").innerHTML = "<hr> <p> Analyse de la question en cours ... Application des règles </p>"
        const promise_regles = await fetch(LOCAL_URL + '/regles_questions.txt')
        const fichier_regles = await promise_regles.text()
        let lignes_fichier_regles = fichier_regles.split('\n')
        let regles = []
        for(ligne of lignes_fichier_regles) {
            if(!ligne.startsWith("#") && ligne.length > 0) {
                regles.push(ruleFromRawLine(ligne))
            }
        }
        let keep_going = true
        while(keep_going) {
            keep_going = false
            for(regle of regles) {
                keep_going = questionGraph.apply(regle) || keep_going 
            }
        }
        document.getElementById("log_question").innerHTML = "<hr> <p> Analyse effectuée </p>"
        console.log(questionGraph)
        let questions = questionGraph.nodes.find(n => n.label == "Question:").inVertices
        debug("Nombre d'interprétations possibles pour la question: " + questions.length)
        for(question of questions) {
            debug(question.initialNode.label)
        }
        
        answerQuestion(questions)

    } else {
        document.getElementById("log_phrase").innerHTML = "<hr> <p> Analyse de la phrase en cours ... Application des règles </p>"
        const promise_regles = await fetch(LOCAL_URL + '/regles.txt')
        const fichier_regles = await promise_regles.text()
        let lignes_fichier_regles = fichier_regles.split('\n')
        let regles = []
        for(ligne of lignes_fichier_regles) {
            if(!ligne.startsWith("#") && ligne.length > 0) {
                regles.push(ruleFromRawLine(ligne))
            }
        }
        let keep_going = true
        while(keep_going) {
            keep_going = false
            for(regle of regles) {
                keep_going = sentenceGraph.apply(regle) || keep_going 
            }
        }
        document.getElementById("log_phrase").innerHTML = "<hr> <p> Analyse effectuée </p>"
        console.log(sentenceGraph)
        let phrases = sentenceGraph.nodes.find(n => n.label == "Phrase:").inVertices
        debug("Nombre d'interprétations possibles pour la phrase: " + phrases.length)
        for(phrase of phrases) {
            debug(phrase.initialNode.label)
        }
    }    
}

function answerQuestion(questions) {

    if(questions.length >= 1) {
        document.getElementById("log_reponse").innerHTML = "<p> Calcul de la réponse ...</p>"
    } else {
        document.getElementById("log_reponse").innerHTML = "<p> Je n'ai pas bien compris la question </p>"
        return
    }

    let question = questions[0].initialNode

    let mot_interrogatif = undefined
    let action = undefined
    let complement = undefined

    try { mot_interrogatif = question.outVertices.find(v => v.type == "r_interro").endNode.label } catch(e) { console.log("Mot interrogatif introuvable ..."); return };
    try { action = question.outVertices.find(v => v.type == "r_action").endNode.label } catch(e) { console.log("Action introuvable ..."); return };
    try { complement = question.outVertices.find(v => v.type == "r_patient").endNode.outVertices.find(v=> v.type == "r_head").endNode.label } catch(e) { console.log("Complement introuvable ...") };

    console.log(mot_interrogatif, action, complement);

    // on tente de répondre avec la phrase donnée

    let rules = []
    let conditions = []
    switch(mot_interrogatif) {
        case 'qui':
            conditions.push(new RulePart("$phrase", "r_pos", "Phrase:"))
            conditions.push(new RulePart("$phrase", "r_action", action))
            if(complement !== undefined) {
                conditions.push(new RulePart("$phrase", "r_patient", "$p"))
                conditions.push(new RulePart("$p", "r_head", complement))
            }
            conditions.push(new RulePart("$phrase", "r_agent", "$a"))
            conditions.push(new RulePart("$a", "r_head", "$reponse"))
            rules.push(new Rule(conditions, []))
            break;
        case 'que':
            conditions.push(new RulePart("$phrase", "r_pos", "Phrase:"))
            conditions.push(new RulePart("$phrase", "r_action", action))
            if(complement !== undefined) {
                conditions.push(new RulePart("$phrase", "r_patient", "$p"))
                conditions.push(new RulePart("$p", "r_head", "$reponse"))
            } else {
                document.getElementById("log_reponse").innerHTML = "<p> Il me manque le complement pour répondre </p>"
                return;
            }
            conditions.push(new RulePart("$phrase", "r_agent", "$a"))
            conditions.push(new RulePart("$a", "r_head", complement))
            rules.push(new Rule(conditions, []))
            break;
        case 'comment':
            if(action == "est") {
                // l'agent
                conditions.push(new RulePart("$phrase", "r_pos", "Phrase:"))
                if(complement !== undefined) {
                    conditions.push(new RulePart("$phrase", "r_agent", "$a"))
                    conditions.push(new RulePart("$a", "r_head", complement))
                    conditions.push(new RulePart("$a", "r_carac", "$reponse"))
                } else {
                    document.getElementById("log_reponse").innerHTML = "<p> Il me manque le complement pour répondre </p>"
                    return;
                }
                rules.push(new Rule(conditions, []))

                // le patient
                conditions = []
                conditions.push(new RulePart("$phrase", "r_pos", "Phrase:"))
                if(complement !== undefined) {
                    conditions.push(new RulePart("$phrase", "r_patient", "$p"))
                    conditions.push(new RulePart("$p", "r_head", complement))
                    conditions.push(new RulePart("$p", "r_carac", "$reponse"))
                } else {
                    document.getElementById("log_reponse").innerHTML = "<p> Il me manque le complement pour répondre </p>"
                    return;
                }
                rules.push(new Rule(conditions, []))
            }
            break;
        default:
    }

    let string_reponse = ""
    for(rule of rules) {
        let res_backtrackall = backtrackAll(sentenceGraph.vertices, {}, rule, 0, [])
        console.log(res_backtrackall, rule)
        res_backtrackall.forEach(reponse => {
            string_reponse += "<p>" + reponse['$reponse'].label.toString() + "</p> <hr>"
        })
    }
    if(string_reponse != "") {
        document.getElementById("log_reponse").innerHTML = string_reponse
    } else {
        // pas sur la phrase de base
        // On cherche si la question a un rapport avec la phrase donnée
        let link = "";
        if(complement !== undefined) {
            link = sentenceGraph.nodes.find(n => n.label==complement)
        }
        if(link == undefined) {
            document.getElementById("log_reponse").innerHTML = "Cette question n'a pas vraiment de rapport avec la phrase donnée"
            debug("Raison : " + complement + " non présent dans la phrase initiale")
            return;
        }
        link = sentenceGraph.nodes.filter(n => n.label=="Phrase:").filter(n=>n.inVertices[0].initialNode.outVertices.find(v=>v.type=="r_action").endNode.label==action)
        if(link.length == 0 && action != "est") {
            document.getElementById("log_reponse").innerHTML = "Cette question n'a pas vraiment de rapport avec la phrase donnée"
            debug("Raison : " + action + " non présent dans la phrase initiale")
            return;
        }

        // On cherche la réponse en utilisant les aggrégats
        document.getElementById("log_reponse").innerHTML = "Je ne sais pas mais je me renseigne un peu plus ..."

        let action_lemmes = undefined
        try { action_lemmes = question.outVertices.find(v=>v.type=="r_action").endNode.outVertices.filter(v=>v.type=="r_lemma") } catch(e) { console.log("Action introuvable ..."); return };
        let lemme = action_lemmes[0]
        let lemme_label = lemme.endNode.label
        let recherche_type = ""
        console.log(lemme_label, action_lemmes.length);
        switch (mot_interrogatif) {
            case 'que':
                recherche_type = " [objet] "
                break;
            case 'comment':
                if(action == "est") {
                    recherche_type = " [carac] "
                    lemme_label = complement
                } else {
                    recherche_type = " [avec] "
                }
                break;
            case 'où':
                recherche_type = " [lieu] "
                lemme_label = complement
                break;
            default:
                break;
        }
        try {
            questionGraph
            .nodes
            .find(n => n.label==complement)
            .outVertices
            .filter(v => v.type=="r_idee_jdm" && v.endNode.label.indexOf(lemme_label + recherche_type) != -1)
            .forEach(v => {
                string_reponse += "<p>" + v.endNode.label.split(' ')[v.endNode.label.split(' ').length-1] + "</p> <hr>";
                debug("Raison : " + v.endNode.label)
            })
        } catch(e) {document.getElementById("log_reponse_jdm").innerHTML = "Je ne sais vraiment pas, désolé"}
        if(string_reponse == "") {
            document.getElementById("log_reponse_jdm").innerHTML = "Je ne sais vraiment pas, désolé"
        } else {
            document.getElementById("log_reponse_jdm").innerHTML = string_reponse
        }
    }
}

function repondre() {    
    document.getElementById("log_question").innerHTML = "<hr> <p> Analyse de la question en cours ... Requêtage</p>"
    document.getElementById("log_reponse").innerHTML = ""
    document.getElementById("log_reponse_jdm").innerHTML = ""

    let question = document.getElementById('phrase_question').value.replace(',', ' , ').replace('\'', '\' ').split(" ")
    question[0] = question[0].toLowerCase()
    if(question[0] == "avec" && question[1] == "quoi") {
        question.splice(1,1)
        question[0] = "comment"
    }
    if(question[0] == "ou") {
        question[0] = "où"
    }

    generateGraph(question, true)
    
    let deja_vu = []
    for (let mot of question) {
        if(deja_vu.indexOf(mot) == -1) {
            deja_vu.push(mot)
            requestJDM(mot, 4, true) // POS
            requestJDM(mot, 19, true) // Lemmes
            requestJDM(mot, 18, true) // r_data
            requestJDM(mot, 0, true) // idée associée
            requestJDM(mot, 13, true) // r_agent (ce que peut faire mot)
            requestJDM(mot, 14, true) // r_patient (ce qu'on peut faire à mot)
        }
    }
}