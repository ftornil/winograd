const express = require('express')
var app = express()
const path = require('path')
const PORT = process.env.PORT || 5000
const fs = require('fs');
const XMLHttpRequest = require('xhr2');

const JDM_URL = "http://www.jeuxdemots.org/rezo-dump.php?gotermsubmit=Chercher&gotermrel=%s&rel=%s"

var cors = require('cors')
app.use(cors())

app.use(express.static(path.join(__dirname, '.')))
app.set('views', path.join(__dirname, 'html'))
app.set('view engine', 'html')

app.get('/', (req, res) => res.render('index'))

app.get('/data', (req, res) => {
    let rel = req.query['rel']
    let mot = req.query['mot']
    fs.readFile('cache/'+mot+'_'+rel, (err, data) => {
        if (err) {
            let url = JDM_URL.replace('%s', mot).replace('%s', rel)
            let request = new XMLHttpRequest();
            request.open('GET', url, true)
            request.overrideMimeType('text/html; charset=iso-8859-1')
            request.onload = function() {
                fs.writeFile('cache/'+mot+'_'+rel, request.responseText, () => {} )
                res.write(request.responseText)
                res.end()
            }
            request.send(null)
        } else {
            res.write(data)
            res.end()
        }
    });
})

app.listen(PORT, () => console.log('Client running on port ' + PORT))
