# Projet Winograd

## Une base pour le projet

- [X] Champ de saisie d'une phrase
- [X] Classes graphe de base
- [X] Parser la phrase sous forme de graphe (r_succ & r_pred)
- [X] Requêter JDM
- [X] Parse résultat JDM
- [X] Ajouter les relations grammaticales (r_pos) (jdm)

## Traiter JDM (de gentille petite base de connaissance)

- [X] Faire le lien relation entité
- [X] Traiter les poids (négatif)
- [X] ...

## Un début de réflexion

- [] Définir les règles (GN - Sauter les éléments optionnels (adjectifs, adverbes, compléments))
- [X] Appliquer les règles (Markov (il faut faire une boucle))
- [] Création de nouveaux noeuds par une règle
- [X] Lemmatiser
- [] Gestion des mots composés
- [] Mots avec ' du
- [] Voix passive
- [] Desambiguïser
- [] Coréférence
- [] qui ?

## Une analyse des questions

- [X] Lemmatiser la question : qui dort -> qui dormir
- [X] déterminants

## Bugs


## Des questions plus complexes

## Une réponse aux questions

